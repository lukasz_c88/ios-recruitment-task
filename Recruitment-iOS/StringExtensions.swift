//
//  StringExtensions.swift
//  Recruitment-iOS
//
//  Created by user187707 on 3/1/21.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import Foundation

extension String {
    func withUpperCaseAtEvenIndicesAndLowerAtOddOnes() -> String {
        return self.enumerated().reduce("") { (partialResult, element) -> String in
            let newLetter = (element.offset % 2 == 1) ? String(element.element).lowercased() : String(element.element).uppercased()
            return partialResult.appending(newLetter)
        }
    }
}
