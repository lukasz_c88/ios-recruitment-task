//
//  TableViewCell.swift
//  Recruitment-iOS
//
//  Created by user187707 on 2/25/21.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var previewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static func cellID() -> String {
        return "TableViewCellID"
    }
}
