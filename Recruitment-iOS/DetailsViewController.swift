//
//  DetailsViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, NetworkingManagerDelegate {
    
    @IBOutlet weak var textView: UITextView!
    var itemModel: ItemModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.parent?.navigationController?.navigationBar.isHidden = true

        guard let itemModel = self.itemModel else {
            // TODO: log/print/debugassert
            return
        }
        
        self.title = itemModel.name.withUpperCaseAtEvenIndicesAndLowerAtOddOnes()
        self.view.backgroundColor = itemModel.color

        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItemWithID(itemModel.id)
    }

    func downloadedItems(_ items: [ItemModel]) {
        
    }
    
    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?) {
        guard let details = itemDetails else {
            return
        }
        textView.text = details.desc
    }

}
