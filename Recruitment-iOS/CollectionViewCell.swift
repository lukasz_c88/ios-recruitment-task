//
//  CollectionViewCell.swift
//  Recruitment-iOS
//
//  Created by user187707 on 2/24/21.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var previewLabel: UILabel!
    
    static func reuseID() -> String {
        return "CollectionViewCellID"
    }
}
