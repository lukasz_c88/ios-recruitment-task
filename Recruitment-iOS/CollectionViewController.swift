//
//  CollectionViewController.swift
//  Recruitment-iOS
//
//  Created by user187707 on 2/24/21.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import UIKit

private let reuseIdentifier = CollectionViewCell.reuseID()

class CollectionViewController: UICollectionViewController, NetworkingManagerDelegate, UICollectionViewDelegateFlowLayout {

    var itemModels = [ItemModel]()
    private static let itemsPerRow = 2
    private static let sectionInsets = UIEdgeInsets(
      top: 50.0,
      left: 20.0,
      bottom: 50.0,
      right: 20.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.estimatedItemSize = CGSize.zero;
        }
        
        collectionView.allowsSelection = true
        collectionView.allowsMultipleSelection = false;
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.parent?.navigationController?.navigationBar.isHidden = false
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemModels.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        // Configure the cell
        guard let collectionViewCell = cell as? CollectionViewCell else {
            // TODO: log/print/debugassert
            return cell
        }
        
        let itemModel = itemModels[indexPath.row]
        collectionViewCell.titleLabel.text = itemModel.name
        collectionViewCell.previewLabel.text = itemModel.preview
        collectionViewCell.backgroundColor = itemModel.color
        
        return collectionViewCell
    }

    // MARK: UICollectionViewDelegateFlowLayout

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let paddingSpace = CollectionViewController.sectionInsets.left * CGFloat(CollectionViewController.itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / CGFloat(CollectionViewController.itemsPerRow)

        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return CollectionViewController.sectionInsets
      }


      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CollectionViewController.sectionInsets.left
      }
    
    // MARK: NetworkingManagerDelegate
    
    func downloadedItems(_ items: [ItemModel]) {
        self.itemModels = items
        self.collectionView.reloadData()
    }
    
    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?) {
        
    }
    
    // MARK: passing item model to DetailsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailsVC = segue.destination as? DetailsViewController else {
            // TODO: log/print/debugassert
            return
        }
        
        guard let selectedIndexPaths = collectionView.indexPathsForSelectedItems else {
            // TODO: log/print/debugassert
            return
        }

        guard 1 == selectedIndexPaths.count else {
            // TODO: log/print/debugassert
            return
        }
        
        guard let selectedIndexPath = selectedIndexPaths.first else {
            // TODO: log/print/debugassert
            return
        }
        
        detailsVC.itemModel = itemModels[selectedIndexPath.item]
    }
}
