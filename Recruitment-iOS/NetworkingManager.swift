//
//  NetworkingManager.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

protocol ItemModelJSONSource: AnyObject {
    func getItemModelsJSON(completionBlock:@escaping ([String : Any]) -> Void)
    func getDetailModelJSON(id: String, completionBlock:@escaping ([String : Any]) -> Void)
}

class MainBundleFileItemModelJSONSource: ItemModelJSONSource {
    
    private let itemsFileName: String
    private let itemFileBuilder: (String) -> String
    
    init(itemsFileName: String, itemFileBuilder: @escaping (String) -> String) {
        self.itemsFileName = itemsFileName
        self.itemFileBuilder = itemFileBuilder
    }
    
    func getItemModelsJSON(completionBlock: @escaping ([String : Any]) -> Void) {
        getJSON(filename: self.itemsFileName) { (json) in
            completionBlock(json)
        }
    }
    
    func getDetailModelJSON(id: String, completionBlock: @escaping ([String : Any]) -> Void) {
        let filename = self.itemFileBuilder(id)
        getJSON(filename: filename) { (json) in
            completionBlock(json)
        }
    }
    
    private func getJSON(filename: String, completionBlock: @escaping ([String : Any]) -> Void) {
        if let dictionary = JSONParser.jsonFromFilename(filename) {
            completionBlock(dictionary)
        } else {
            completionBlock([:])
        }
    }
}

protocol NetworkingManagerDelegate: AnyObject {
    
    func downloadedItems(_ items:[ItemModel])
    func downloadedItemDetails(_ itemDetails:ItemDetailsModel?)
    
}

class NetworkingManager: NSObject {
    private static let colorNameToColor = [
        "Red" : UIColor.red,
        "Green" : UIColor.green,
        "Blue" : UIColor.blue,
        "Yellow" : UIColor.yellow,
        "Purple" : UIColor.purple,
    ];
    private static let defaultColor = UIColor.black
    
    static var sharedManager = NetworkingManager()
    
    weak var delegate:NetworkingManagerDelegate?
    private static let defaultJSONSource = MainBundleFileItemModelJSONSource(itemsFileName: "Items.json") { (id) -> String in
        return "Item\(id).json"
    }
    weak var jsonSource: ItemModelJSONSource? = NetworkingManager.defaultJSONSource
    
    func downloadItems() {
        request() { dictionary in
            let data = dictionary["data"]
            var result:[ItemModel] = []
            guard let array = data as? [[String : Any]] else {
                // TODO: print/log/debugAssert
                self.delegate?.downloadedItems(result)
                return
            }
            
            for item in array {
                guard let id = item["id"] as? String else {
                    continue
                }
                
                var name = ""
                var preview = ""
                var colorString = ""
                
                if let attributes = item["attributes"] as? [String : Any] {
                    if let nameObj = attributes["name"] as? String {
                        name = nameObj
                    } else {
                        // TODO: print/log/debugAssert
                    }
                    
                    if let colorStringObj = attributes["color"] as? String {
                        colorString = colorStringObj
                    } else {
                        // TODO: print/log/debugAssert
                    }
                    
                    if let previewObj = attributes["preview"] as? String {
                        preview = previewObj
                    } else {
                        // TODO: print/log/debugAssert
                    }
                } else {
                    // TODO: print/log/debugAssert
                }
                
                let color = NetworkingManager.colorNameToColor[colorString] ?? NetworkingManager.defaultColor
                let itemModel = ItemModel(id: id, name: name, preview: preview, color: color)
                result.append(itemModel)
            }
            self.delegate?.downloadedItems(result)
        }
    }
    
    func downloadItemWithID(_ id:String) {
        request(id: id) { dictionary in
            let data = dictionary["data"]
            guard let dataDict = data as? [String : Any] else {
                // TODO: print/log/debugAssert
                self.delegate?.downloadedItemDetails(nil)
                return
            }
            
            guard let idObj = dataDict["id"] as? String else {
                // TODO: print/log/debugAssert
                self.delegate?.downloadedItemDetails(nil)
                return
            }
            
            guard id == idObj else {
                // TODO: print/log/debugAssert
                self.delegate?.downloadedItemDetails(nil)
                return
            }
            
            var name = ""
            var desc = ""
            var colorString = ""
            
            if let attributes = dataDict["attributes"] as? [String : Any] {
                if let nameObj = attributes["name"] as? String {
                    name = nameObj
                } else {
                    // TODO: print/log/debugAssert
                }
                
                if let colorStringObj = attributes["color"] as? String {
                    colorString = colorStringObj
                } else {
                    // TODO: print/log/debugAssert
                }
                
                if let descObj = attributes["desc"] as? String {
                    desc = descObj
                } else {
                    // TODO: print/log/debugAssert
                }
            } else {
                // TODO: print/log/debugAssert
            }
            
            let color = NetworkingManager.colorNameToColor[colorString] ?? NetworkingManager.defaultColor
            let itemModelDetails = ItemDetailsModel(id: id, name: name, preview: "", color: color, desc: desc)
            self.delegate?.downloadedItemDetails(itemModelDetails)
        }
    }
    
    private func request(id:String? = nil, completionBlock:@escaping ([String : Any]) -> Void) {
        
        guard let jsonSource = self.jsonSource else {
            // TODO: print/log/debugAssert
            completionBlock([:])
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let id = id {
                jsonSource.getDetailModelJSON(id: id) { (jsonDict) in
                    completionBlock(jsonDict)
                }
            } else {
                jsonSource.getItemModelsJSON { (jsonDict) in
                    completionBlock(jsonDict)
                }
            }
        }
    }
}
