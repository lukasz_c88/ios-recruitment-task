//
//  TableViewController.swift
//  UKiOSTest
//
//  Created by Paweł Sporysz on 15.09.2016.
//  Copyright © 2016 Paweł Sporysz. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, NetworkingManagerDelegate {
    
    var itemModels:[ItemModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelection = false
        tableView.allowsSelection = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100.0
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.parent?.navigationController?.navigationBar.isHidden = false
        NetworkingManager.sharedManager.delegate = self
        NetworkingManager.sharedManager.downloadItems()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellID", for: indexPath)
        
        guard let tableViewCell = cell as? TableViewCell else {
            // TODO: print/log/debugassert
            return cell
        }
        
        let itemModel = itemModels[indexPath.row]
        tableViewCell.backgroundColor = itemModel.color
        tableViewCell.nameLabel.text = itemModel.name
        tableViewCell.previewLabel.text = itemModel.preview
        
        return tableViewCell
    }
    
    func downloadedItems(_ items: [ItemModel]) {
        self.itemModels = items
        self.tableView.reloadData()
    }
    
    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?) {
        
    }
    
    // MARK: passing item model to DetailsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailsVC = segue.destination as? DetailsViewController else {
            // TODO: log/print/debugassert
            return
        }
        
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else {
            // TODO: log/print/debugassert
            return
        }
        
        detailsVC.itemModel = itemModels[selectedIndexPath.row]
    }
}
