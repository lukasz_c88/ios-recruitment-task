//
//  StringExtensionsTests.swift
//  Recruitment-iOSTests
//
//  Created by user187707 on 3/2/21.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class StringExtensionsTests: XCTestCase {

    func testEmptyString() {
        XCTAssertEqual("".withUpperCaseAtEvenIndicesAndLowerAtOddOnes(), "")
    }

    func testStringContainingNonCharacters() {
        let testString = "1_ "
        XCTAssertEqual(testString.withUpperCaseAtEvenIndicesAndLowerAtOddOnes(), testString)
    }

    func testStringContainingUpperCaseCharactersOnly() {
        XCTAssertEqual("ABC".withUpperCaseAtEvenIndicesAndLowerAtOddOnes(), "AbC")
    }

    func testStringContainingLowerCaseCharactersOnly() {
        XCTAssertEqual("abc".withUpperCaseAtEvenIndicesAndLowerAtOddOnes(), "AbC")
    }

    func testStringContainingOnlyOneLetterWichIsUpperCase() {
        let testString = "A"
        XCTAssertEqual(testString.withUpperCaseAtEvenIndicesAndLowerAtOddOnes(), testString)
    }

    func testStringContainingOnlyOneLetterWichIsLowerCase() {
        XCTAssertEqual("a".withUpperCaseAtEvenIndicesAndLowerAtOddOnes(), "A")
    }

    func testStringContainingMiscellaneousCharacters() {
        XCTAssertEqual("aBCd_X12 !".withUpperCaseAtEvenIndicesAndLowerAtOddOnes(), "AbCd_x12 !")
    }
}
