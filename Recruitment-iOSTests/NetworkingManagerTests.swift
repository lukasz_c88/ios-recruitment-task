//
//  NetworkingManagerTests.swift
//  Recruitment-iOSTests
//
//  Created by user187707 on 3/2/21.
//  Copyright © 2021 Untitled Kingdom. All rights reserved.
//

import XCTest
@testable import Recruitment_iOS

class StringItemModelJSONSource: ItemModelJSONSource {
    var itemsJSONString: String
    var itemJSONString: String
    
    init(itemsJSONString: String, itemJSONString: String) {
        self.itemsJSONString = itemsJSONString
        self.itemJSONString = itemJSONString
    }
    
    func getItemModelsJSON(completionBlock: @escaping ([String : Any]) -> Void) {
        getJSON(for: self.itemsJSONString, completionBlock: completionBlock)
    }
    
    func getDetailModelJSON(id: String, completionBlock: @escaping ([String : Any]) -> Void) {
        getJSON(for: self.itemJSONString, completionBlock: completionBlock)
    }
    
    private func getJSON(for jsonString: String, completionBlock: @escaping ([String : Any]) -> Void) {
        guard let data = jsonString.data(using: String.Encoding.utf8) else {
            completionBlock([:])
            return
        }
        
        guard let jsonDict = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String : Any] else {
            completionBlock([:])
            return
        }
        
        completionBlock(jsonDict)
    }
}

class NetworkingManagerTests: XCTestCase, NetworkingManagerDelegate {
    let jsonSource = StringItemModelJSONSource(itemsJSONString: "", itemJSONString: "")
    let manager = NetworkingManager.sharedManager
    var currentExpectation: XCTestExpectation = XCTestExpectation(description: "initial")
    var itemDetailsModel: ItemDetailsModel?
    var itemModels: [ItemModel]?
    
    override func setUp() {
        super.setUp()
        self.manager.delegate = self
        self.manager.jsonSource = self.jsonSource
        self.itemModels = nil
        self.itemDetailsModel = nil
        self.jsonSource.itemJSONString = ""
        self.jsonSource.itemsJSONString = ""
    }
    
    // MARK: downloadIteWithID()
    
    func testFetchingItemDetailsModel() {
        self.currentExpectation = self.expectation(description: "fetching details model")
        let itemID = "1"
        self.jsonSource.itemJSONString = "{\"data\":{\"id\":\"\(itemID)\",\"type\":\"ItemDetails\",\"attributes\":{\"name\":\"Item1\",\"color\":\"Green\",\"desc\":\"x\"}}}"
        //self.jsonSource.itemsJSONString = ""
        self.manager.downloadItemWithID(itemID)
        waitForExpectations(timeout: 5.0, handler: nil)
        let detailsExpected = ItemDetailsModel(id: itemID, name: "Item1", preview: "", color: UIColor.green, desc: "x")
        
        guard let itemDetailsModel = self.itemDetailsModel else {
            fatalError("Item details model should not be nil")
        }
        
        XCTAssertTrue(detailsExpected.equal(to: itemDetailsModel))
    }
    
    func testFetchingItemDetailsModelIfStringContainsEmptyJSON() {
        self.currentExpectation = self.expectation(description: "fetching details model for string with empty json")
        self.jsonSource.itemJSONString = "{}"
        self.manager.downloadItemWithID("1")
        waitForExpectations(timeout: 5.0, handler: nil)
        XCTAssertNil(self.itemDetailsModel)
    }
    
    func testFetchingItemDetailsModelIfStringIsMissingNameKey() {
        self.currentExpectation = self.expectation(description: "fetching details model for string with missing name key")
        let itemID = "1"
        self.jsonSource.itemJSONString = "{\"data\":{\"id\":\"\(itemID)\",\"type\":\"ItemDetails\",\"attributes\":{\"color\":\"Green\",\"desc\":\"x\"}}}"
        //self.jsonSource.itemsJSONString = ""
        self.manager.downloadItemWithID(itemID)
        waitForExpectations(timeout: 5.0, handler: nil)
        let detailsExpected = ItemDetailsModel(id: itemID, name: "", preview: "", color: UIColor.green, desc: "x")
        
        guard let itemDetailsModel = self.itemDetailsModel else {
            fatalError("Item details model should not be nil")
        }
        
        XCTAssertTrue(detailsExpected.equal(to: itemDetailsModel))
    }
    
    // MARK: downloadItems()
    
    func testFetchingItemModels() {
        self.currentExpectation = self.expectation(description: "fetching item models")
        self.jsonSource.itemsJSONString = "{\"data\":[{\"id\":\"2\",\"type\":\"Items\",\"attributes\":{\"name\":\"Item2\",\"preview\":\"x\",\"color\":\"Red\"}},{\"id\":\"1\",\"type\":\"Items\",\"attributes\":{\"name\":\"Item1\",\"preview\":\"y\",\"color\":\"Green\"}}]}"
        self.manager.downloadItems()
        waitForExpectations(timeout: 5.0, handler: nil)
        let itemsExpected = [
            ItemModel(id: "2", name: "Item2", preview: "x", color: UIColor.red),
            ItemModel(id: "1", name: "Item1", preview: "y", color: UIColor.green),
        ]
        
        guard let itemModels = self.itemModels else {
            fatalError("Item models should not be nil")
        }
        
        XCTAssertTrue(NetworkingManagerTests.itemModelArraysEqual(lhs: itemsExpected, rhs: itemModels))
    }
    
    func testFetchingItemModelsIfStringIsEmptyJSON() {
        self.currentExpectation = self.expectation(description: "fetching item models if string is empty JSON")
        self.jsonSource.itemsJSONString = "{}"
        self.manager.downloadItems()
        waitForExpectations(timeout: 5.0, handler: nil)
        let itemsExpected = [ItemModel]()
        
        guard let itemModels = self.itemModels else {
            fatalError("Item models should not be nil")
        }
        
        XCTAssertTrue(NetworkingManagerTests.itemModelArraysEqual(lhs: itemsExpected, rhs: itemModels))
    }
    
    static func itemModelArraysEqual(lhs: [ItemModel], rhs: [ItemModel]) -> Bool {
        guard lhs.count == rhs.count else {
            return false
        }
        
        for index in 0..<lhs.count {
            if !lhs[index].equal(to: rhs[index]) {
                return false
            }
        }
        
        return true
    }
    
    // MARK: NetworkingManagerDelegate
    
    func downloadedItems(_ items: [ItemModel]) {
        self.itemModels = items
        self.currentExpectation.fulfill()
    }
    
    func downloadedItemDetails(_ itemDetails: ItemDetailsModel?) {
        self.itemDetailsModel = itemDetails
        self.currentExpectation.fulfill()
    }
}
